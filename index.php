<?php
session_start();
require 'functions.php';

if (isset($_COOKIE['id_user']) && isset($_COOKIE['key'])) {
    $id = $_COOKIE['id_user'];
    $key = $_COOKIE['key'];

    $result = mysqli_query($conn, "SELECT * FROM 
    tb_user WHERE id_user = $id");
    $row = mysqli_fetch_assoc($result);


    if ($key === hash('sha256', $row['username'])) {
        $_SESSION['login'] = true;
        $_SESSION['id_user'] = $row['id_user'];
        $_SESSION['nama_user'] = $row['nama_user'];
        $_SESSION['username'] = $row['username'];
        $_SESSION['password'] = $row['password'];
        $_SESSION['id_outlet'] = $row['id_outlet'];
        $_SESSION['level'] = $row['level'];
    }
}


if (isset($_SESSION["login"])) {
    if ($row['level'] === 'Kasir') {
        header("location: kasir/index.php");
        exit;
    } elseif ($row['level'] === 'Owner') {
        header("location: owner/index.php");
        exit;
    }
    header("location: admin/index.php");
    exit;
}

if (isset($_POST["login"])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $result = mysqli_query($conn, "SELECT * FROM tb_user WHERE 
    username = '$username'");
    if (mysqli_num_rows($result) === 1) {

        $row = mysqli_fetch_assoc($result);
        if (password_verify($password, $row["password"])) {

            $_SESSION["login"] = true;

            $_SESSION['id_user'] = $row['id_user'];
            $_SESSION['nama_user'] = $row['nama_user'];
            $_SESSION['username'] = $row['username'];
            $_SESSION['password'] = $row['password'];
            $_SESSION['id_outlet'] = $row['id_outlet'];
            $_SESSION['level'] = $row['level'];

            if (isset($_POST['remember'])) {
                setcookie('id_user', $row['id_user'], time() + 60);
                setcookie('key', hash('sha256', $row['username']), time() + 60);
            }

            if ($row['level'] === 'Kasir') {
                header("location: kasir/index.php");
                exit;
            } elseif ($row['level'] === 'Owner') {
                header("location: owner/index.php");
                exit;
            }

            header("location: admin/index.php");
            exit;
        }
    }
    $error = true;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LAUNDRY WEBSITE</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<style>
    body {
        background-image: url(bg.jpg);
        background-size: cover;
        position: relative;
    }
</style>

<body class="login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card card-outline card-primary" style="background: rgba(255, 255, 255, .1);">
            <div class="card-header text-center" style="color:azure;">
                <h1><b>LAUNDRY WEBSITE</b></h1>
                <?php if (isset($error)) : ?>
                    <p style="color: red; font-style:italic;">Username / Password Salah</p>
                <?php endif; ?>
            </div>
            <div class="card-body">

                <form action="" method="post">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Username" name="username" id="password" required style="background: rgba(255, 255, 255, .1); ">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group ">
                        <input type="password" class="form-control" placeholder="Password" name="password" id="password" required style="background: rgba(255, 255, 255, .1);">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="form-check ml-1 mb-2">
                            <input type="checkbox" class="form-check-input mt-2" id="remember" name="remember">
                            <label class="form-check-label" for="remember" style="color: azure;">Ingat Saya</label>
                        </div>
                    </div>
                    <div>
                        <!-- /.col -->
                        <div>
                            <button type="submit" name="login" class="btn btn-primary btn-block ">Masuk</button>
                        </div>
                        <hr>

                        <a href="">
                            <center>Buat Akun</center>
                        </a>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
</body>

</html>