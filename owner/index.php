<?php include("navbar.php") ?>
<?php include("sidebar.php") ?>
<?php

if (!isset($_SESSION["login"])) {
  header("location: ../index.php");
  exit;
}

$get1 = mysqli_query($conn, "SELECT * FROM tb_pelanggan");
$jumlahpelanggan = mysqli_num_rows($get1);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-primary text-center">
            <div class="inner">
              <h3><i class="fas fa-user fa-xs"></i> <?= $jumlahpelanggan; ?></h3>
              <p>Jumlah Pelanggan</p>
            </div>
            <!-- <a href="#" class="small-box-footer">More primary <i class="fas fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger text-center">
            <div class="inner">
              <h3><i class="fas fa-sync-alt fa-xs"></i> 15</h3>
              <p>Tugas Berjalan</p>
            </div>
            <!-- <a href="#" class="small-box-footer">More primary <i class="fas fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info text-center">
            <div class="inner">
              <h3>20</h3>
              <p>Laporan Berjalan</p>
            </div>
            <!-- <a href="#" class="small-box-footer">More primary <i class="fas fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success text-center">
            <div class="inner">
              <h3>20</h3>
              <p>Laporan Berjalan</p>
            </div>
            <!-- <a href="#" class="small-box-footer">More primary <i class="fas fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <?php
            if (isset($_SESSION['flash'])) {
              echo $_SESSION['flash'];
              unset($_SESSION['flash']);
            } ?>
            <h3 class="card-title mt-2">
              <i class="far fa-bell mr-1"></i>
              <b>Notifikasi</b>
            </h3>
            <div class="card-tools mt-2">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                <div class="input-group-append">
                  <button type="submit" class="btn btn-default">
                    <i class="fas fa-search"></i>
                  </button>
                </div>
              </div>
            </div>
          </div><!-- /.card-header -->
          <div class="card-body table-responsive p-0" style="height: 300px;">
            <table class="table table-head-fixed text-nowrap">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tugas</th>
                  <th>OPD</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>183</td>
                  <td>John Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="tag tag-success">Approved</span></td>
                </tr>
                <tr>
                  <td>219</td>
                  <td>Alexander Pierce</td>
                  <td>11-7-2014</td>
                  <td><span class="tag tag-warning">Pending</span></td>
                </tr>
                <tr>
                  <td>657</td>
                  <td>Bob Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="tag tag-primary">Approved</span></td>
                </tr>
                <tr>
                  <td>175</td>
                  <td>Mike Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="tag tag-danger">Denied</span></td>
                </tr>
                <tr>
                  <td>134</td>
                  <td>Jim Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="tag tag-success">Approved</span></td>
                </tr>
                <tr>
                  <td>494</td>
                  <td>Victoria Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="tag tag-warning">Pending</span></td>
                </tr>
                <tr>
                  <td>832</td>
                  <td>Michael Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="tag tag-primary">Approved</span></td>
                </tr>
                <tr>
                  <td>982</td>
                  <td>Rocky Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="tag tag-danger">Denied</span></td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
<?php include("footer.php") ?>