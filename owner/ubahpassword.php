<?php include("navbar.php") ?>
<?php include("sidebar.php") ?>
<?php
if (!isset($_SESSION["login"])) {
    header("location: ../index.php");
    exit;
}


$id = $_GET["id_user"];

$laundry = query("SELECT * FROM tb_user INNER JOIN tb_outlet ON tb_user.id_outlet = tb_outlet.id_outlet WHERE id_user = $id ")[0];



?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Admin</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">Master Data</a></li> -->
                        <li class="breadcrumb-item active">Management Laporan</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

    <div class="content">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-7">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <?php if (isset($_POST["submit"])) {
                                            ubahpassword($_POST);
                                        }
                                        ?>
                                        <form action="" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="id_user" value="<?= $laundry['id_user']; ?>">
                                            <input type="hidden" name="nama_user" value="<?= $laundry['nama_user']; ?>">
                                            <input type="hidden" name="username" value="<?= $laundry['username']; ?>">
                                            <input type="hidden" name="id_outlet" value="<?= $laundry['id_outlet']; ?>">
                                            <input type="hidden" name="level" value="<?= $laundry['level']; ?>">
                                            <div class="form-group">
                                                <label for="passwordlama">Password Lama</label>
                                                <input type="password" class="form-control" id="passwordlama" name="passwordlama" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password Baru</label>
                                                <input type="password" class="form-control" id="password" name="password" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="password1">Konfirmasi Password Baru</label>
                                                <input type="password" class="form-control" id="password1" name="password1" required>
                                            </div>
                                            <div>
                                                <button type="submit" name="submit" class="btn btn-primary">Ubah</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-body">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<?php include("footer.php") ?>