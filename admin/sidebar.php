<?php
$link = $_SERVER['PHP_SELF'];
$link_array = explode('/', $link);
$page = end($link_array);
if (!isset($_SESSION["login"])) {
    header("location: ../index.php");
    exit;
} ?>
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link text-center">
        <!-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span> -->
        <span class="brand-text font-weight-bold">LAUNDRY WEBSITE</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <!-- <a href="./index.php" class="nav-link active"> -->
                    <a href="index.php" class="nav-link <?= $page == 'index.php' ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p><b>
                                DASHBOARD
                            </b></p>
                    </a>
                <li class="nav-item">
                    <a href="data_pengguna.php" class="nav-link <?= $page == 'data_pengguna.php' ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-folder"></i>
                        <p><b>
                                DATA PENGGUNA
                            </b></p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="data_paket.php" class="nav-link <?= $page == 'data_paket.php' ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-folder"></i>
                        <p><b>
                                DATA PAKET
                            </b></p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="data_outlet.php" class="nav-link <?= $page == 'data_outlet.php' ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-folder"></i>
                        <p><b>
                                DATA OUTLET
                            </b></p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="data_pelanggan.php" class="nav-link <?= $page == 'data_pelanggan.php' ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-folder"></i>
                        <p><b>
                                DATA PELANGGAN
                            </b></p>
                    </a>
                </li>
                </li>
                <li class="nav-item <?= $page == 'data_transaksi.php' ? 'menu-open' : '' ?>">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p><b>
                                DATA TRANSAKSI</b>
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="data_transaksi.php" class="nav-link <?= $page == 'data_pengguna.php' ? 'active' : '' ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p><b>OPD</b></p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-bullhorn"></i>
                        <p><b>
                                PENGUMUMAN
                            </b></p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="editprofile.php" class="nav-link <?= $page == 'editprofile.php' ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-cog"></i>
                        <p><b>
                                EDIT PROFILE
                            </b></p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>