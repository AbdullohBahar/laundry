<?php include("navbar.php") ?>
<?php include("sidebar.php") ?>
<?php if (!isset($_SESSION["login"])) {
    header("location: ../index.php");
    exit;
}

$laundry = query("SELECT * FROM tb_transaksi
");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Data Transaksi</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                        <li class="breadcrumb-item active">Data Transaksi</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-end">
                            <h5 class="col">Data Transaksi</h5>
                            <div class="col-0 mr-2"><a href="tambahtransaksi.php">
                                    <button class="btn btn-success btn-sm">
                                        <i class="fas fa-plus"></i> Tambah Transaksi</button></a>
                            </div>

                        </div>
                    </div>
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No Transaksi</th>
                                    <th>No Outlet</th>
                                    <th>No Pelanggan</th>
                                    <th>No Paket</th>
                                    <th>Tanggal Masuk</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Berat Cucian</th>
                                    <th>Total Bayar</th>
                                    <th>Status Bayar</th>
                                    <th>Status Transaksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($laundry as $row) : ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $row[""]; ?></td>
                                    </tr>
                                    <?php $i++ ?>
                                <?php endforeach ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No Transaksi</th>
                                    <th>No Outlet</th>
                                    <th>No Pelanggan</th>
                                    <th>No Paket</th>
                                    <th>Tanggal Masuk</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Berat Cucian</th>
                                    <th>Total Bayar</th>
                                    <th>Status Bayar</th>
                                    <th>Status Transaksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<?php include("footer.php") ?>