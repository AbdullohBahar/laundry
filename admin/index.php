<?php include("navbar.php") ?>
<?php include("sidebar.php") ?>
<?php

if (!isset($_SESSION["login"])) {
  header("location: ../index.php");
  exit;
}

$get1 = mysqli_query($conn, "SELECT * FROM tb_pelanggan");
$jumlahpelanggan = mysqli_num_rows($get1);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6 col-6">
          <!-- small box -->
          <div class="small-box bg-primary text-center">
            <div class="inner">
              <h3><i class="fas fa-user fa-xs"></i> <?= $jumlahpelanggan; ?></h3>
              <p>Jumlah Pelanggan</p>
            </div>
            <!-- <a href="#" class="small-box-footer">More primary <i class="fas fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <div class="col-lg-6 col-6">
          <!-- small box -->
          <div class="small-box bg-danger text-center">
            <div class="inner">
              <h3><i class="fas fa-sync-alt fa-xs"></i> 15</h3>
              <p>Tugas Berjalan</p>
            </div>
            <!-- <a href="#" class="small-box-footer">More primary <i class="fas fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-6">
          <!-- small box -->
          <div class="small-box bg-info text-center">
            <div class="inner">
              <h3>20</h3>
              <p>Laporan Berjalan</p>
            </div>
            <!-- <a href="#" class="small-box-footer">More primary <i class="fas fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <div class="col-lg-6 col-6">
          <!-- small box -->
          <div class="small-box bg-success text-center">
            <div class="inner">
              <h3>20</h3>
              <p>Laporan Berjalan</p>
            </div>
            <!-- <a href="#" class="small-box-footer">More primary <i class="fas fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
<?php include("footer.php") ?>