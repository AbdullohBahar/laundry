<?php


session_start();
$_SESSION = [];
session_unset();
session_destroy();

unset($_COOKIE['id_user']);
unset($_COOKIE['key']);

setcookie('id_user', '', time() - 100);
setcookie('key', '', time() - 100);

header("location: ../index.php");
exit;
