<?php include("navbar.php") ?>
<?php include("sidebar.php") ?>
<?php
if (!isset($_SESSION["login"])) {
    header("location: ../index.php");
    exit;
}
// cek apakah tombol submit sudah ditekan
// if (isset($_POST["submit"])) {

//     // cek apakah data berhasil di tambahkan atau tidak
//     if ( tpbaru($_POST) > 0) {
//         echo "
//             <script>
//                 alert('data berhasil ditambahkan!');
//                 document.location.href = 'index.php';
//             </script>
//         ";
//     }  else{
//         echo "
//             <script>
//                 alert('data gagal ditambahkan!');
//                 document.location.href = 'index.php';
//             </script>
//         ";
//     }

// }



?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Admin</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">Master Data</a></li> -->
                        <li class="breadcrumb-item active">Management Laporan</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

    <div class="content">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-7">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <?php if (isset($_POST["submit"])) {
                                            // cek apakah data berhasil di edit atau tidak
                                            if (tpbaru($_POST) > 0) {
                                                // Set session flash                                                    
                                                $_SESSION['flash'] =
                                                    // Nah iki message e sing mengko mbok gawe bedo"
                                                    '<div class="alert alert-success alert-dismissible fade show" role="alert">
                                                         Pelanggan baru berhasil ditambahkan.
                                                         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                         <span aria-hidden="true">&times;</span>
                                                         </button>
                                                         </div>';
                                                echo "
                                                                <script>
                                                                    document.location.href = 'data_pelanggan.php';
                                                                </script>
                                                                ";
                                            } else {
                                                echo "
                                                                    <script>
                                                                        alert('data gagal diubah!');
                                                                        document.location.href = 'data_pelanggan.php';
                                                                    </script>
                                                                ";
                                            }
                                        } ?>
                                        <form action="" method="POST" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="nama_pelanggan">Nama Pelanggan</label>
                                                <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" placeholder="Masukkan Nama...">
                                            </div>
                                            <div class="form-group">
                                                <label for="alamat">Alamat Pelanggan</label>
                                                <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukkan Alamat...">
                                            </div>
                                            <div class="form-group">
                                                <label for="telp">Nomor Pelanggan</label>
                                                <input type="text" class="form-control" id="telp" name="telp" placeholder="Masukkan Nomor Handphone...">
                                            </div>
                                            <div class="form-group">
                                                <label for="id_outlet">OUTLET</label>
                                                <select class="form-control" id="id_outlet" name="id_outlet">
                                                    <option value="">- Pilih -</option>
                                                    <?php
                                                    $sql_outlet = mysqli_query($conn, "SELECT * FROM tb_outlet") or die(mysqli_error($conn));
                                                    while ($data_outlet = mysqli_fetch_array($sql_outlet)) {
                                                        echo '<option value="' . $data_outlet['id_outlet'] . '">' . $data_outlet['nama_outlet'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="jenis_langganan">Jenis Langganan</label>
                                                <select class="form-control" id="jenis_langganan" name="jenis_langganan">
                                                    <option value="">- Pilih -</option>
                                                    <option value="Member">Member</option>
                                                    <option value="Reguler">Reguler</option>
                                                </select>
                                            </div>
                                            <button type="submit" name="submit" class="btn btn-primary">tambahkan</button>
                                        </form>
                                    </div>
                                    <div class="card-body">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<?php include("footer.php") ?>