<?php include("navbar.php") ?>
<?php include("sidebar.php") ?>
<?php
if (!isset($_SESSION["login"])) {
    header("location: ../index.php");
    exit;
}

$id = $_GET["id_outlet"];

$laundry = query("SELECT * FROM tb_outlet WHERE id_outlet = $id")[0];

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Admin</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">Master Data</a></li> -->
                        <li class="breadcrumb-item active">Management Laporan</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

    <div class="content">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-7">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <form action="" method="POST" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <?php if (isset($_POST["submit"])) {
                                                    // cek apakah data berhasil di edit atau tidak
                                                    if (ubahoutlet($_POST) > 0) {

                                                        $_SESSION['flash'] =
                                                            ' <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                                                        Data Outlet berhasil diubah.
                                                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                                            <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>';
                                                        echo "
                                                                <script>
                                                                    document.location.href = 'data_outlet.php';
                                                                </script>
                                                                ";

                                                        // }
                                                        // echo ' <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                        //                         Data Outlet berhasil diubah.
                                                        //                         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        //                             <span aria-hidden="true">&times;</span>
                                                        //                         </button>
                                                        //                     </div>';
                                                        //                     exit;

                                                    } else {
                                                        echo "
                                                                    <script>
                                                                        alert('data gagal diubah!');
                                                                        document.location.href = 'index.php';
                                                                    </script>
                                                                ";
                                                    }
                                                } ?>
                                                <label for="id_outlet">ID OUTLET</label>
                                                <input type="text" class="form-control" id="id_outlet" name="id_outlet" required value="<?= $laundry["id_outlet"]; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama_outlet">Nama Outlet</label>
                                                <input type="text" class="form-control" id="nama_outlet" name="nama_outlet" required value="<?= $laundry["nama_outlet"]; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="alamat">Alamat Outlet</label>
                                                <input type="text" class="form-control" id="alamat" name="alamat" required value="<?= $laundry["alamat"]; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="telp">Nomor Handphone Outlet</label>
                                                <input type="text" class="form-control" id="telp" name="telp" required value="<?= $laundry["telp"]; ?>">
                                            </div>
                                            <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                                        </form>
                                    </div>
                                    <div class="card-body">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<?php include("footer.php") ?>