<?php
$conn = mysqli_connect("localhost", "root", "", "laundry");


function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}


function tpbaru($data)
{
    global $conn;
    // ambil data dari tiap element dalam form
    $nama_pelanggan = htmlspecialchars($data["nama_pelanggan"]);
    $alamat = htmlspecialchars($data["alamat"]);
    $telp = htmlspecialchars($data["telp"]);
    $id_outlet = htmlspecialchars($data["id_outlet"]);
    $jenis_langganan = htmlspecialchars($data["jenis_langganan"]);

    // query insert data
    $query = "INSERT INTO tb_pelanggan
                VALUES
                ('', '$nama_pelanggan', '$alamat', '$telp', '$id_outlet', '$jenis_langganan' )
                ";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function tbaru($data)
{
    global $conn;
    // ambil data dari tiap element dalam form
    $nama_pelanggan = htmlspecialchars($data["nama_pelanggan"]);
    $alamat = htmlspecialchars($data["alamat"]);
    $telp = htmlspecialchars($data["telp"]);
    $id_outlet = htmlspecialchars($data["id_outlet"]);
    $jenis_langganan = htmlspecialchars($data["jenis_langganan"]);

    // query insert data
    $query = "INSERT INTO tb_pelanggan
                VALUES
                ('', '$nama_pelanggan', '$alamat', '$telp', '$id_outlet', '$jenis_langganan' )
                ";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function tapeng($data)
{
    global $conn;
    // ambil data dari tiap element dalam form
    $nama_user = htmlspecialchars($data["nama_user"]);
    $username = strtolower(stripslashes($data["username"]));
    $password = mysqli_real_escape_string($conn, $data["password"]);
    $password2 = mysqli_real_escape_string($conn, $data["password2"]);
    $id_outlet = htmlspecialchars($data["id_outlet"]);
    $level = htmlspecialchars($data["level"]);

    if ($password !== $password2) {
        $_SESSION['flash'] =
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">
           Konfirmasi password tidak sesuai.
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
          </div>';
        echo "
           <script>
            document.location.href = 'data_pengguna.php';
          </script>
          ";
        return false;
    }
    $password = password_hash($password, PASSWORD_DEFAULT);
    // query insert data
    $query = "INSERT INTO tb_user
            VALUES
           ('', '$nama_user', '$username', '$password', '$id_outlet', '$level' )
           ";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function tobaru($data)
{
    global $conn;
    // ambil data dari tiap element dalam form
    $id_outlet = htmlspecialchars($data["id_outlet"]);
    $nama_outlet = htmlspecialchars($data["nama_outlet"]);
    $alamat = htmlspecialchars($data["alamat"]);
    $telp = htmlspecialchars($data["telp"]);

    // query insert data
    $query = "INSERT INTO tb_outlet
                VALUES
                ('$id_outlet', '$nama_outlet', '$alamat', '$telp' )
                ";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}
function tpaketbaru($data)
{
    global $conn;
    // ambil data dari tiap element dalam form
    $id_outlet = htmlspecialchars($data["id_outlet"]);
    $jenis = htmlspecialchars($data["jenis"]);
    $harga = htmlspecialchars($data["harga"]);

    // query insert data
    $query = "INSERT INTO tb_paket
                VALUES
                ('', '$id_outlet', '$jenis', '$harga' )
                ";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function ubahpelanggan($data)
{
    global $conn;
    // ambil data dari tiap element dalam form
    $id_pelanggan = htmlspecialchars($data["id_pelanggan"]);
    $nama_pelanggan = htmlspecialchars($data["nama_pelanggan"]);
    $alamat = htmlspecialchars($data["alamat"]);
    $telp = htmlspecialchars($data["telp"]);
    $id_outlet = htmlspecialchars($data["id_outlet"]);
    $jenis_langganan = htmlspecialchars($data["jenis_langganan"]);


    // query update data
    $query = "UPDATE tb_pelanggan SET
                id_pelanggan = '$id_pelanggan',
                nama_pelanggan = '$nama_pelanggan',
                alamat = '$alamat',
                telp = '$telp',
                id_outlet = '$id_outlet',
                jenis_langganan = '$jenis_langganan'
                WHERE id_pelanggan = $id_pelanggan
                ";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function ubahpengguna($data)
{
    global $conn;
    // ambil data dari tiap element dalam form
    $id_user = htmlspecialchars($data["id_user"]);
    $nama_user = htmlspecialchars($data["nama_user"]);
    $username = htmlspecialchars($data["username"]);
    $password = htmlspecialchars($data["password"]);
    $id_outlet = htmlspecialchars($data["id_outlet"]);
    $level = htmlspecialchars($data["level"]);


    // query update data
    $query = "UPDATE tb_user SET
                id_user = '$id_user',
                nama_user = '$nama_user',
                username = '$username',
                password = '$password',
                id_outlet = '$id_outlet',
                level = '$level'
                WHERE id_user = $id_user
                ";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function ubahoutlet($data)
{
    global $conn;
    // ambil data dari tiap element dalam form
    $id_outlet = htmlspecialchars($data["id_outlet"]);
    $nama_outlet = htmlspecialchars($data["nama_outlet"]);
    $alamat = htmlspecialchars($data["alamat"]);
    $telp = htmlspecialchars($data["telp"]);

    // query update data
    $query = "UPDATE tb_outlet SET
                id_outlet = '$id_outlet',
                nama_outlet = '$nama_outlet',
                alamat = '$alamat',
                telp = '$telp'
                WHERE id_outlet = $id_outlet
                ";
    // var_dump($query);
    // die;
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function ubahpassword($data)
{
    global $conn;

    // ambil data dari tiap element dalam form
    $id_user = htmlspecialchars($data["id_user"]);
    $nama_user = htmlspecialchars($data["nama_user"]);
    $username = htmlspecialchars($data["username"]);
    $passwordlama = mysqli_real_escape_string($conn, $data["passwordlama"]);
    $password = mysqli_real_escape_string($conn, $data["password"]);
    $password1 = mysqli_real_escape_string($conn, $data["password1"]);
    $id_outlet = htmlspecialchars($data["id_outlet"]);
    $level = htmlspecialchars($data["level"]);
    $queryUser = mysqli_query($conn, "SELECT * FROM tb_user WHERE id_user = $id_user");
    $panggil = mysqli_fetch_assoc($queryUser);

    if (!password_verify($passwordlama, $panggil["password"])) {
        $_SESSION['flash'] =
            // Nah iki message e sing mengko mbok gawe bedo"
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                 Password lama salah.
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
                 </button>
                 </div>';
        echo "
                 <script>
                 document.location.href = 'editprofile.php';
                 </script>
                 ";
    } else {
        if ($password1 == $password) {

            $password = password_hash($password, PASSWORD_DEFAULT);
            // query update data
            $query = "UPDATE tb_user SET
                        id_user = '$id_user',
                        nama_user = '$nama_user',
                        username = '$username',
                        password = '$password',
                        id_outlet = '$id_outlet',
                        level = '$level'
                        WHERE id_user = $id_user
                        ";
            mysqli_query($conn, $query);
            return mysqli_affected_rows($conn);
            $_SESSION['flash'] =
                // Nah iki message e sing mengko mbok gawe bedo"
                '<div class="alert alert-success alert-dismissible fade show" role="alert">
                 Password berhasil diubah.
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
                 </button>
                 </div>';
            echo "
                 <script>
                 document.location.href = 'editprofile.php';
                 </script>
                 ";
        } else {
            $_SESSION['flash'] =
                // Nah iki message e sing mengko mbok gawe bedo"
                '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                 Konfirmasi password salah.
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
                 </button>
                 </div>';
            echo "
                 <script>
                 document.location.href = 'editprofile.php';
                 </script>
                 ";
        }
    }
}


function ubahprofile($data)
{
    global $conn;
    // ambil data dari tiap element dalam form
    $id_user = htmlspecialchars($data["id_user"]);
    $nama_user = htmlspecialchars($data["nama_user"]);
    $username = htmlspecialchars($data["username"]);
    $password1 = htmlspecialchars($data["password1"]);
    $id_outlet = htmlspecialchars($data["id_outlet"]);
    $level = htmlspecialchars($data["level"]);


    // query update data
    $query = "UPDATE tb_user SET 
                nama_user = '$nama_user',
                username = '$username',
                password = '$password1',
                id_outlet = '$id_outlet',
                level = '$level'
                WHERE id_user = $id_user
                ";
    // var_dump($query);
    // die;
    if (mysqli_affected_rows($conn) >= 0) {
        // Timpa session lamamu
        $_SESSION['id_user'] = $id_user;
        $_SESSION['nama_user'] = $nama_user;
        $_SESSION['username'] = $username;
        $_SESSION['password'] = $password1;
        $_SESSION['id_outlet'] = $id_outlet;
        $_SESSION['level'] = $level;
    }
    mysqli_query($conn, $query) or die(mysqli_error($conn));
    return mysqli_affected_rows($conn);
    mysqli_query($conn, $query);
    // Cek berhasil ngubah po ora


}

function hpoutlet($id_outlet)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM tb_outlet WHERE id_outlet = $id_outlet");
    return mysqli_affected_rows($conn);
}
function hppelanggan($id_pelanggan)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM tb_pelanggan WHERE id_pelanggan = $id_pelanggan");
    return mysqli_affected_rows($conn);
}
