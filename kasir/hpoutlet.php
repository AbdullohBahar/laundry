<?php
session_start();
if (!isset($_SESSION["login"])) {
    header("location: ../index.php");
    exit;
} ?>
<?php
require 'functions.php';

$id_outlet = $_GET["id_outlet"];
if (hpoutlet($id_outlet) > 0) {
    $_SESSION['flash'] =
        ' <div class="alert alert-success alert-dismissible fade show" role="alert">
             Data Outlet berhasil dihapus.
             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
                </div>';
    echo "
                <script>
                    document.location.href = 'data_outlet.php';
                </script>
            ";
} else {
    echo "
                <script>
                    alert('data gagal ditambahkan!');
                    document.location.href = 'data_outlet.php';
                </script>
            ";
}
