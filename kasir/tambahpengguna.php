<?php include("navbar.php") ?>
<?php include("sidebar.php") ?>
<?php
if (!isset($_SESSION["login"])) {
    header("location: ../index.php");
    exit;
}
// cek apakah tombol submit sudah ditekan
// if (isset($_POST["submit"])) {

//     // cek apakah data berhasil di tambahkan atau tidak
//     if ( tpbaru($_POST) > 0) {
//         echo "
//             <script>
//                 alert('data berhasil ditambahkan!');
//                 document.location.href = 'index.php';
//             </script>
//         ";
//     }  else{
//         echo "
//             <script>
//                 alert('data gagal ditambahkan!');
//                 document.location.href = 'index.php';
//             </script>
//         ";
//     }

// }



?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Admin</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">Master Data</a></li> -->
                        <li class="breadcrumb-item active">Management Laporan</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

    <div class="content">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-7">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <?php if (isset($_POST["submit"])) {
                                            // cek apakah data berhasil di edit atau tidak
                                            if (tapeng($_POST) > 0) {
                                                // Set session flash                                                    
                                                $_SESSION['flash'] =
                                                    // Nah iki message e sing mengko mbok gawe bedo"
                                                    '<div class="alert alert-success alert-dismissible fade show" role="alert">
                                                         Pengguna baru berhasil ditambahkan.
                                                         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                         <span aria-hidden="true">&times;</span>
                                                         </button>
                                                         </div>';
                                                echo "
                                                                <script>
                                                                    document.location.href = 'data_pengguna.php';
                                                                </script>
                                                                ";
                                            } else {
                                                echo "
                                                                    <script>
                                                                        alert('data gagal diubah!');
                                                                        document.location.href = 'data_pengguna.php';
                                                                    </script>
                                                                ";
                                            }
                                        } ?>
                                        <form action="" method="POST" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="nama_user">Nama Pengguna</label>
                                                <input type="text" class="form-control" id="nama_user" name="nama_user" placeholder="Masukkan Nama..." required>
                                            </div>
                                            <div class="form-group">
                                                <label for="username">Username</label>
                                                <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan Username..." required>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Password..." required>
                                            </div>
                                            <div class="form-group">
                                                <label for="password2">Password</label>
                                                <input type="password" class="form-control" id="password2" name="password2" placeholder="Masukkan Konfirmasi Password..." required>
                                            </div>
                                            <div class="form-group">
                                                <label for="id_outlet">OUTLET</label>
                                                <select class="form-control" id="id_outlet" name="id_outlet" required>
                                                    <option value="">- Pilih -</option>
                                                    <?php
                                                    $sql_outlet = mysqli_query($conn, "SELECT * FROM tb_outlet") or die(mysqli_error($conn));
                                                    while ($data_outlet = mysqli_fetch_array($sql_outlet)) {
                                                        echo '<option value="' . $data_outlet['id_outlet'] . '">' . $data_outlet['nama_outlet'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="level">Hak Akses</label>
                                                <select class="form-control" id="level" name="level" required>
                                                    <option value="">- Pilih -</option>
                                                    <option value="Admin">Admin</option>
                                                    <option value="Kasir">Kasir</option>
                                                    <option value="Owner">Owner</option>
                                                </select>
                                            </div>
                                            <button type="submit" name="submit" class="btn btn-primary">tambahkan</button>
                                        </form>
                                    </div>
                                    <div class="card-body">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<?php include("footer.php") ?>