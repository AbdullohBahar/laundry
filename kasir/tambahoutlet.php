<?php include("navbar.php") ?>
<?php include("sidebar.php") ?>
<?php
if (!isset($_SESSION["login"])) {
    header("location: ../index.php");
    exit;
}
// cek apakah tombol submit sudah ditekan
// if (isset($_POST["submit"])) {

//     // cek apakah data berhasil di tambahkan atau tidak
//     if ( tpbaru($_POST) > 0) {
//         echo "
//             <script>
//                 alert('data berhasil ditambahkan!');
//                 document.location.href = 'index.php';
//             </script>
//         ";
//     }  else{
//         echo "
//             <script>
//                 alert('data gagal ditambahkan!');
//                 document.location.href = 'index.php';
//             </script>
//         ";
//     }

// }



?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Admin</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">Master Data</a></li> -->
                        <li class="breadcrumb-item active">Management Laporan</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

    <div class="content">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-7">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <?php if (isset($_POST["submit"])) {
                                            // cek apakah data berhasil di edit atau tidak
                                            if (tobaru($_POST) > 0) {
                                                // Set session flash                                                    
                                                $_SESSION['flash'] =
                                                    // Nah iki message e sing mengko mbok gawe bedo"
                                                    '<div class="alert alert-success alert-dismissible fade show" role="alert">
                                                                                        Outlet baru berhasil ditambahkan.
                                                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                                            <span aria-hidden="true">&times;</span>
                                                                                        </button>
                                                                                    </div>';
                                                echo "
                                                                <script>
                                                                    document.location.href = 'data_outlet.php';
                                                                </script>
                                                                ";
                                            } else {
                                                echo "
                                                                    <script>
                                                                        alert('data gagal diubah!');
                                                                        document.location.href = 'data_outlet.php';
                                                                    </script>
                                                                ";
                                            }
                                        } ?>
                                        <form action="" method="POST" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="id_outlet">ID OUTLET</label>
                                                <input type="text" class="form-control" id="id_outlet" name="id_outlet" placeholder="Masukkan ID OUTLET...">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama_outlet">Nama Outlet</label>
                                                <input type="text" class="form-control" id="nama_outlet" name="nama_outlet" placeholder="Masukkan Nama Outlet..." required>
                                            </div>
                                            <div class="form-group">
                                                <label for="alamat">Alamat Outlet</label>
                                                <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukkan Alamat Outlet..." required>
                                            </div>
                                            <div class="form-group">
                                                <label for="telp">Nomor Handphone Outlet</label>
                                                <input type="text" class="form-control" id="telp" name="telp" placeholder="Masukkan Nomor Handphone Outlet...">
                                            </div>
                                            <button type="submit" name="submit" class="btn btn-primary">tambahkan</button>
                                        </form>
                                    </div>
                                    <div class="card-body">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<?php include("footer.php") ?>