<?php include("navbar.php") ?>
<?php include("sidebar.php") ?>
<?php
if (!isset($_SESSION["login"])) {
    header("location: ../index.php");
    exit;
}
$laundry = query("SELECT * FROM tb_outlet");


?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $_SESSION['level']; ?></h1>
                    <h4>
                        <small>Data Outlet</small>
                        <div class="pull-right">
                        </div>
                    </h4>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">Master Data</a></li> -->
                        <li class="breadcrumb-item active">Data Outlet</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-11">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <?php
                                        if (isset($_SESSION['flash'])) {
                                            echo $_SESSION['flash'];
                                            unset($_SESSION['flash']);
                                        }
                                        ?>
                                        <div class="row justify-content-end">
                                            <h5 class="col">Data Outlet</h5>
                                            <!-- <div class="col-0 mr-2"><a href="tambahoutlet.php">
                                                    <button class="btn btn-success btn-sm">
                                                        <i class="fas fa-plus"></i> Tambah Outlet Baru</button></a>
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Outlet</th>
                                                    <th>Alamat Outlet</th>
                                                    <th>Nomor Telepon</th>
                                                    <!-- <th>Aksi</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($laundry as $row) : ?>
                                                    <tr>
                                                        <td><?= $row['id_outlet']; ?></td>
                                                        <td><?= $row['nama_outlet']; ?></td>
                                                        <td><?= $row['alamat']; ?></td>
                                                        <td><?= $row['telp']; ?></td>
                                                        <!-- <td><a href="edoutlet.php?id_outlet=<?= $row["id_outlet"]; ?>"><button class="btn btn-primary btn-sm">Edit <i class="far fa-edit"></i></button></a>
                                                            <a href="hpoutlet.php?id_outlet=<?= $row["id_outlet"]; ?>"><button class="btn btn-danger btn-sm">Hapus <i class="far fa-trash-alt"></i></button></a>
                                                        </td> -->
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Outlet</th>
                                                    <th>Alamat Outlet</th>
                                                    <th>Nomor Telepon</th>
                                                    <!-- <th>Aksi</th> -->
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<?php include("footer.php") ?>