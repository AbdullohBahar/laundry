<?php include("navbar.php") ?>
<?php include("sidebar.php") ?>
<?php
if (!isset($_SESSION["login"])) {
    header("location: ../index.php");
    exit;
}

$id = $_SESSION['id_user'];


$laundry = query("SELECT * FROM tb_user INNER JOIN tb_outlet ON tb_user.id_outlet = tb_outlet.id_outlet WHERE id_user = $id ")[0];
// $id = $_GET["id_pelanggan"];

// $laundry = query("SELECT * FROM tb_pelanggan WHERE id_pelanggan = $id")[0];

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0"><?= $_SESSION['level']; ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <!-- <li class="breadcrumb-item"><a href="#">Master Data</a></li> -->
                        <li class="breadcrumb-item active">Edit Profile</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

    <div class="content">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-7">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <?php
                                        if (isset($_SESSION['flash'])) {
                                            echo $_SESSION['flash'];
                                            unset($_SESSION['flash']);
                                        }
                                        ?>
                                        <?php if (isset($_POST["submit"])) {
                                            // cek apakah data berhasil di edit atau tidak
                                            if (ubahprofile($_POST) > 0) {
                                                // Set session flash                                                    
                                                $_SESSION['flash'] =
                                                    // Nah iki message e sing mengko mbok gawe bedo"
                                                    '<div class="alert alert-success alert-dismissible fade show" role="alert">
                                                         User berhasil diubah.
                                                         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                         <span aria-hidden="true">&times;</span>
                                                         </button>
                                                         </div>';
                                                echo "
                                                                <script>
                                                                    document.location.href = 'index.php';
                                                                </script>
                                                                ";
                                            } else {
                                                $_SESSION['flash'] =
                                                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                                 User gagal diubah.
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                                </div>';
                                                echo "
                                                                        <script>
                                                                            document.location.href = 'index.php';
                                                                        </script>
                                                                    ";
                                            }
                                        }
                                        ?>
                                        <form action="" method="POST" enctype="multipart/form-data">
                                            <input type="hidden" name="id_user" value="<?= $_SESSION['id_user']; ?>">
                                            <input type="hidden" name="password1" value="<?= $_SESSION['password']; ?>">
                                            <div class="form-group">
                                                <label for="nama_user">Nama Lengkap</label>
                                                <input type="text" class="form-control" id="nama_user" name="nama_user" required value="<?= $_SESSION['nama_user']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="username">Username</label>
                                                <input type="text" class="form-control" id="username" name="username" required value="<?= $_SESSION['username']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="id_outlet">ID OUTLET</label>
                                                <select class="form-control" id="id_outlet" name="id_outlet" required>
                                                    <option value="<?= $_SESSION['id_outlet']; ?>"><?= $laundry['nama_outlet']; ?></option>
                                                    <?php
                                                    $sql_outlet = mysqli_query($conn, "SELECT * FROM tb_outlet") or die(mysqli_error($conn));
                                                    while ($data_outlet = mysqli_fetch_array($sql_outlet)) {
                                                        echo '<option value="' . $data_outlet['id_outlet'] . '">' . $data_outlet['nama_outlet'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="level">Level</label>
                                                <select class="form-control" id="level" name="level" required>
                                                    <option value="<?= $_SESSION['level']; ?>"><?= $_SESSION['level']; ?></option>
                                                    <option value="Admin">Admin</option>
                                                    <option value="Kasir">Kasir</option>
                                                    <option value="Owner">Owner</option>
                                                </select>
                                            </div>
                                            <button type="submit" name="submit" class="btn btn-primary">Ubah</button>
                                            <hr>
                                            <a href="ubahpassword.php?id_user=<?= $laundry["id_user"]; ?>">Ubah Password</a>
                                        </form>
                                    </div>
                                    <div class="card-body">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<?php include("footer.php") ?>