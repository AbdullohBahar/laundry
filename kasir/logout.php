<?php


session_start();
$_SESSION = [];
session_unset();
session_destroy();

setcookie('id_user', '', 1);
setcookie('key', '', 1);

unset($_COOKIE['id_user']);
unset($_COOKIE['key']);

header("location: ../index.php");
exit;
